<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Auth;
use Hash;
use App\SMS;
use App\Responseobject;
use Response;
use App\Otps;
use App\Loans;
use App\Savings;
use App\Groupmembers;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName','lastName','mobileNo','isOnline','isDisabled','userType', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public static function checkAccess($hashedKey) {

      return true;

    }

    public static function getUserBymobileNo($mobileNo) {
      $appurl = User::getMainURL();
        $udata = User::select('id','mobileNo','email','firstName','lastName','profilePic','userType','isOnline')
        ->where('mobileNo',$mobileNo)->where('isDeleted',0)->first();
        if($udata) {
        return array('id'=>$udata->id,'firstName'=>$udata->firstName,'lastName'=>$udata->lastName,'mobileNo'=>$udata->mobileNo,'email'=>$udata->email,'userType'=>$udata->userType,'isOnline'=>$udata->isOnline);
      }
      else {
        return false;
      }
    }

    public static function getUserById($id) {
        return User::select('id','countyId','wardId','dob','nationalId','kraPin','hobby','funny','mobileNo','email','firstName','gender','profilePic','lastName','userType','isOnline','created_at')
        ->where('id',$id)->where('isDeleted',0)->first();
    }

    public static function myProfile($userId) {
      $groups = Groupmembers::where('userId',$userId)->where('isDeleted',0)->groupBy('userId')->count();
      $savings = Savings::where('userId',$userId)->where('isDeleted',0)->groupBy('userId')->sum('amount');
      $loan = Loans::where('userId',$userId)->where('status','active')->where('isDeleted',0)->first();
      if($loan) {
        $loanAmount = $loan->amount;
      }
      else {
        $loanAmount = "0.00";
      }

      $user = self::getUserById($userId);
      $profilePic = User::getMainURL()."photos/".$user->profilePic;

      return array('id'=>$userId,'profilePic'=>$profilePic,'dob'=>$user->dob,'wardId'=>$user->wardId,'countyId'=>$user->countyId,'funny'=>$user->funny,'hobby'=>$user->hobby,'kraPin'=>$user->kraPin,'nationalId'=>$user->nationalId,'firstName'=>$user->firstName,'lastName'=>$user->lastName,'mobileNo'=>$user->mobileNo,'gender'=>$user->gender,'groups'=>$groups,'loanAmount'=>$loanAmount,'savings'=>$savings);

    }

    public static function loginUser($mobileNo,$password)
    {
        $mobileNo = "254".substr($mobileNo, -9);
		    $userDetails = User::where('mobileNo',$mobileNo)->first();

        //if (Auth::attempt(['email' => $email, 'password' => $password])) {
			if (Hash::check($password, $userDetails->password)) {

            //make user active
            User::where('mobileNo', $mobileNo)->update(['isOnline' => 1]);

            return true;
        } else {
            return false;
        }
    }

    public static function registerUser($mobileNo,$firstName,$lastName,$dob,$gender,$countyId,$wardId,$kraPin,$nationalId,$hobby,$funny,$password) {

      $model = new User;
      $model->userType = 1;
      $model->gender = $gender;
      $model->password = Hash::make($password);
      $model->firstName = $firstName;
      $model->lastName = $lastName;
      $model->mobileNo = $mobileNo;
      $model->dob = $dob;
      $model->countyId = $countyId;
      $model->wardId = $wardId;
      $model->kraPin = $kraPin;
      $model->nationalId = $nationalId;
      $model->hobby = $hobby;
      $model->funny = $funny;
      $model->save();
      if($model) {

        $content = "Dear ".$firstName." ".$lastName.", Welcome to GottaSave. You have successfully signed up. Proceed to login.";

        SMS::sendSMS($mobileNo,$content);
        Otps::requestOtp($mobileNo);

        return $model->id;
      }
      else {
        return false;
      }

    }

    public static function forgotpass($mobileNo) {
      $ii= strtoupper(str_shuffle(time().$mobileNo."abcdefghijklmnopqrstuvwxyz"));
      $password = substr($ii, -6);
      $model = User::where('mobileNo', $mobileNo)->update(['password' => Hash::make($password)]);
      if ($model) {
        SMS::sendSMS($mobileNo,"Your new password is ".$password."");
        return true;
      }
      else {
        return false;
      }
    }

    public static function editProfile($id,$firstName,$lastName,$dob,$gender,$countyId,$wardId,$kraPin,$nationalId,$hobby,$funny) {
      $response = new Responseobject;
      $model = User::find($id);
      $model->gender = $gender;
      $model->firstName = $firstName;
      $model->lastName = $lastName;
      $model->dob = $dob;
      $model->countyId = $countyId;
      $model->wardId = $wardId;
      $model->kraPin = $kraPin;
      $model->nationalId = $nationalId;
      $model->hobby = $hobby;
      $model->funny = $funny;
      $model->save();
      if($model) {
        $response->status = $response::status_ok;
        $response->code = $response::code_ok;
        $response->message = "Your profile was updated successfully";
        $response->result = null;
      }
      else {
        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = "Failed to update your profile. Please try again";
        $response->result = null;
      }
        return Response::json($response);
    }

    /**
      * Update
      */
      public static function changePassword($userId,$newpassword,$oldpassword)
      {
        $response = new Responseobject;

        $hashedpassword = Hash::make($newpassword);

          $checkdata =User::where('id', $userId)->first();
          if(!Hash::check($oldpassword, $checkpassword)){
            $response->status = $response::status_fail;
            $response->code = $response::code_fail;
            $response->message = "The current password is incorrect";
            $response->result = null;
            }else{
                // write code to update password
                $adddata =User::where('id', $userId)->update(['password' => $hashedpassword]);
                if ($adddata) {
                  $response->status = $response::status_ok;
                  $response->code = $response::code_ok;
                  $response->message = "Your password was changed successfully";
                  $response->result = null;
                } else {
                  $response->status = $response::status_fail;
                  $response->code = $response::code_fail;
                  $response->message = "Failed to change your password. Please try again";
                  $response->result = null;
                }
            }

          return Response::json($response);

      }

    public static function changeStatus($id,$isOnline)
    {
      $response = new Responseobject;
        $model = User::where('id', $id)->update(['isOnline' => $isOnline]);
        if ($model) {
          if($isOnline == 1) {
                  $response->status = $response::status_ok;
                  $response->code = $response::code_ok;
                  $response->message = "You are now ONLINE";
                  $response->result = $isOnline;
                }
                else {
                  $response->status = $response::status_ok;
                  $response->code = $response::code_ok;
                  $response->message = "You are now OFFLINE";
                  $response->result = $isOnline;
                }
                } else {
                  $response->status = $response::status_fail;
                  $response->code = $response::code_fail;
                  $response->message = "Failed to change your status. Please try again";
                  $response->result = $isOnline;
                }
                return Response::json($response);
    }

    public static function requestOtp($hashedKey,$mobileNo,$fcmToken) {

$response = new Responseobject;

  $mobileNo = "254".substr($mobileNo, -9);

  $checkuser = User::where('mobileNo', $mobileNo)->where('isDeleted',0)->first();
  if(!$checkuser) {
    $message = "Mobile number does not exist";
    $response->status = $response::status_fail;
    $response->code = $response::code_fail;
    $response->message = $message;
    $response->result = null;
  }
  else {

    $userId = $checkuser->id;
    $isDisabled = $checkuser->isDisabled;

     if($isDisabled == 1) {
      $message = "Your account is disabled. Contact admin for help";
      $response->status = $response::status_fail;
      $response->code = $response::code_fail;
      $response->message = $message;
      $response->result = null;
    }
    else {
      $message = "Successful. We have sent a one time pin (OTP) to your number ".$mobileNo." Enter it to proceed";
      $otpCode = Otps::requestOtp($mobileNo);
      User::where('mobileNo', $mobileNo)->where('isDeleted',0)->update(['fcmToken' => $fcmToken]);
      $response->status = $response::status_ok;
      $response->code = $response::code_ok;
      $response->message = $message;
      $response->result = $otpCode;
    }

  }

return Response::json($response);
}

public static function storeone($firstName,$lastName,$email,$mobileNo,$roleId,$password)
  {
      $hashedPassword = Hash::make($password);
      $model = new User;
      $model->firstName = $firstName;
      $model->mobileNo = $mobileNo;
      $model->email = $email;
      $model->lastName = $lastName;
      $model->roleId = $roleId;
      $model->userType = 5;
      $model->password = $hashedPassword;
      $model->save();
      if ($model) {
          return true;
      }
      return false;
  }

  public static function updateone($id, $firstName,$lastName,$email,$mobileNo,$gender,$roleId)
  {
      $model = User::find($id);
      $model->firstName = $firstName;
      $model->mobileNo = $mobileNo;
      $model->email = $email;
      $model->lastName = $lastName;
      $model->roleId = $roleId;
      $model->gender = $gender;
      $model->save();
      if ($model) {
          return true;
      }
      return false;
  }

  public static function deleteone($id)
  {
      $model = User::find($id);

      $model->isDeleted = 1;
      $model->save();
      if ($model) {
          return true;
      }
      return false;
  }

  public static function getMainURL()
  {
      return "http://127.0.0.1/gottasave/";
  }

}
