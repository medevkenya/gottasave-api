<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wards extends Model
{

    protected $table = 'wards';

    public static function GetAll($countyId) {
      return Wards::select('wardName','id')->where('countyId',$countyId)->where('isDeleted',0)->get();
    }

}
