<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use App\Responseobject;
use Response;
use Log;
use App\Notifications;
use App\Groups;

class Groupmembers extends Model
{
    protected $table = 'groupmembers';

    public static function joinGroup($userId,$groupId) {
      $response = new Responseobject;

        $checkuser = Groupmembers::where('userId', $userId)->where('groupId',$groupId)->where('status',1)->where('isDeleted',0)->first();
        if($checkuser) {
          $message = "You are already a member of this group";
          $response->status = $response::status_fail;
          $response->code = $response::code_fail;
          $response->message = $message;
          $response->result = null;
        }
        else {

          $countusers = Groupmembers::where('groupId',$groupId)->where('status',1)->where('isDeleted',0)->count();
          $checkgroup = Groups::select('groups.*','grouptypes.capacity')
          ->leftJoin('grouptypes','groups.groupTypeId','=','grouptypes.id')
          ->where('groups.id',$groupId)->where('groups.isDeleted',0)->first();
          if($countusers >= $checkgroup->capacity) {
            $message = "The group is already full";
            $response->status = $response::status_fail;
            $response->code = $response::code_fail;
            $response->message = $message;
            $response->result = null;
          }
          else {

          $model = new Groupmembers;
          $model->userId = $userId;
          $model->groupId = $groupId;
          $model->save();
          if ($model) {
            self::NotifyAllmembers($groupId);
            $response->status = $response::status_ok;
            $response->code = $response::code_ok;
            $response->message = "You have successfully joined the group";
            $response->result = null;
          }
          else {
            $message = "Failed to join group, try again";
            $response->status = $response::status_fail;
            $response->code = $response::code_fail;
            $response->message = $message;
            $response->result = null;
        }
      }
        }
        return Response::json($response);
    }

    public static function exitGroup($id) {
          $response = new Responseobject;

          $model = Groupmembers::where('userId', $userId)->where('groupId',$groupId)->update(['isDeleted' =>1]);
          if ($model) {
            $response->status = $response::status_ok;
            $response->code = $response::code_ok;
            $response->message = "You have successfully exited the group";
            $response->result = null;
          }
          else {
            $message = "Failed to exit group, try again";
            $response->status = $response::status_fail;
            $response->code = $response::code_fail;
            $response->message = $message;
            $response->result = null;
        }

        return Response::json($response);
    }

    public static function groupMembers($userId,$groupId) {
      $data = Groupmembers::select('groupmembers.*','users.firstName','users.lastName','users.profilePic','users.mobileNo','users.gender')
      ->leftJoin('users','groupmembers.userId','=','users.id')
      ->where('groupmembers.groupId',$groupId)
      ->where('groupmembers.isDeleted',0)
      ->get();
      $all = array();
      foreach ($data as $key) {
        $profilePic = User::getMainURL()."photos/".$key->profilePic;
        $memberName = $key->firstName." ".$key->lastName;
        $all[] = array('id'=>$key->id,'userId'=>$key->userId,'mobileNo'=>$key->mobileNo,'gender'=>$key->gender,'memberName'=>$memberName,'profilePic'=>$profilePic);
      }
      return $all;
    }

    public static function NotifyAllmembers($groupId) {
      $all = Groupmembers::where('groupId',$groupId)->where('isDeleted',0)->get();
      foreach ($all as $key) {
        $first = Groups::where('id',$groupId)->first();
        Notifications::CreateNotification($key->userId,"A new member has joined your group. (".$first->groupName.")");
      }
    }

}
