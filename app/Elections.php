<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use App\Responseobject;
use Response;
use Log;
use Carbon\Carbon;

class Elections extends Model
{
    protected $table = 'elections';

    public static function elect($userId,$groupId,$adminId) {
      $response = new Responseobject;

        $checkuser = Elections::where('userId',$userId)->where('groupId',$groupId)
        //->where('adminId',$adminId)
        ->whereDate('created_at', Carbon::today())->where('isDeleted',0)->first();
        if(!$checkuser) {
          $message = "You have already voted today";
          $response->status = $response::status_fail;
          $response->code = $response::code_fail;
          $response->message = $message;
          $response->result = null;
        }
        else
        {
          $model = new Elections;
          $model->userId = $userId;
          $model->groupId = $groupId;
          $model->adminId = $adminId;
          $model->save();
          if ($model) {
            $response->status = $response::status_ok;
            $response->code = $response::code_ok;
            $response->message = "Voted successfully";
            $response->result = null;
          }
          else {
            $message = "Failed to vote, try again";
            $response->status = $response::status_fail;
            $response->code = $response::code_fail;
            $response->message = $message;
            $response->result = null;
        }
        }
        return Response::json($response);
    }

    public static function election($userId,$groupId) {
      $data = Elections::select('elections.*','users.firstName','users.lastName','users.profilePic')
      ->leftJoin('users','elections.adminId','=','users.id')
      ->where('elections.groupId',$groupId)
      ->whereDate('elections.created_at', Carbon::today())
      ->where('elections.isDeleted',0)
      ->groupBy('elections.adminId')->get();
      $all = array();
      foreach ($data as $key) {
        $profilePic = User::getMainURL()."photos/".$key->profilePic;
        $name = $key->firstName." ".$key->lastName;
        $date = self::convertDate($key->created_at);
        $votes = Elections::where('groupId',$groupId)
        ->where('adminId',$key->adminId)
        ->whereDate('created_at', Carbon::today())->where('isDeleted',0)->count();
        $all[] = array('id'=>$key->id,'name'=>$name,'date'=>$date,'votes'=>$votes);
      }
      return $all;
    }

    public static function convertDate($date) {
      return date("d-m-Y H:i A", strtotime($date));
    }


}
