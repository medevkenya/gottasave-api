<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Counties;
use App\Wards;
use Auth;
use Log;
use App\mobile\Responseobject;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Response;

class GeneralController extends Controller {

  /**
* Check Version
*/
public function checkVersion()
{
header('Access-Control-Allow-Origin: *');
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$hashedKey = $request->hashedKey;

if(User::checkAccess($hashedKey)) {

$supported = array("4.2.3");
$supportedIos = array("1.0.0");
$android = array('version' =>"4.2.3",'supported'=>$supported);
$ios = array('version' =>"1.0.0",'supported'=>$supportedIos);

$dsupported = array("1.2.7","1.2.8","1.2.9");
$dsupportedIos = array("1.0.0");
$dandroid = array('version' =>"1.2.9",'supported'=>$dsupported);
$dios = array('version' =>"1.0.0",'supported'=>$dsupportedIos);

$client = array("android" => $android,'ios'=>$ios);
$driver = array("android" => $dandroid,'ios'=>$dios);

$versionData = array("client"=>$client,"driver"=>$driver);

 $dataresponse    = array("client" => $client,'driver'=>$driver,"message" => "Latest App Version","status" => 1);

}
else {
 $dataresponse    = array("message" => "Invalid authentication key","status" => 0);
}

header("Content-Type: application/json");
return json_encode($dataresponse);

}

public function loadCounties()
{
return Counties::GetAll();
}

public function loadWards($countyId)
{
return Wards::GetAll($countyId);
}

}
