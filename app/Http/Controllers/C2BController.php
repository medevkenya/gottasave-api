<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Payment\c2b_payment;
use App\Settings\Invoices;
use App\User;
use App\SMS;

class C2BController extends Controller
{

    /* MPESA To be whitelisted
    196.201.214.200
    196.201.214.206
    196.201.214.207
    196.201.214.208
    196.201.213.44
    196.201.213.114
    */

    /**
    * This is used to generate tokens for the live environment
    * @return mixed
    */
    public static function generateLiveToken() {

        try {
            $consumer_key = env("C2B_CONSUMER_KEY");
            $consumer_secret = env("C2B_CONSUMER_SECRET");
        } catch (\Throwable $th) {
            $consumer_key = self::env("C2B_CONSUMER_KEY");
            $consumer_secret = self::env("C2B_CONSUMER_SECRET");
        }

        if(!isset($consumer_key)||!isset($consumer_secret)){
            die("please declare the consumer key and consumer secret as defined in the documentation");
        }
        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $credentials = base64_encode($consumer_key.':'.$consumer_secret);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curl_response = curl_exec($curl);

        return json_decode($curl_response)->access_token;

    }

    public function registerC2BUrls()
    {
        $live = env("application_status");

        if ($live == "live") {
          $url = 'https://api.safaricom.co.ke/mpesa/c2b/v1/registerurl';
          $token = self::generateLiveToken();
        } elseif ($live == "sandbox") {
          $url = 'https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl';
          $token = self::generateSandBoxToken();
        } else {
          return json_encode(["Message" => "invalid application status"]);
        }

        $ValidationURL = env("C2B_VALIDATION_URL");
        $ConfirmationURL = env("C2B_CONFIRMATION_URL");
        $ResponseType = "Completed";
        $ShortCode = env("C2B_SHORTCODE");

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $token));
        $curl_post_data = array(
          'ValidationURL' => $ValidationURL,
          'ConfirmationURL' => $ConfirmationURL,
          'ResponseType' => $ResponseType,
          'ShortCode' => $ShortCode
        );
        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $curl_response = curl_exec($curl);

        Log::info("raw registerurls---" . json_encode($curl_response));

        var_dump($curl_response);
        //return json_decode($curl_response);
    }

    public function C2Bvalidation()
    {
      header('Access-Control-Allow-Origin: *');
      $postdata = file_get_contents("php://input");
      $request = json_decode($postdata);

      Log::info("raw c2bvalidation---" . json_encode($request));

      if(!empty($request->TransactionType)) {
      $transactionType = $request->TransactionType;
    }
    else {
      $transactionType = 0;
    }
      $transID = $request->TransID;
      $transTime = $request->TransTime;
      $transAmount = $request->TransAmount;
      $businessShortCode = $request->BusinessShortCode;
      $billRefNumber = $request->BillRefNumber;
      $invoiceNumber = $request->InvoiceNumber;
      if(!empty($request->OrgAccountBalance)) {
      $orgAccountBalance = $request->OrgAccountBalance;
    }
    else {
      $orgAccountBalance = 0;
    }
    if(!empty($request->ThirdPartyTransID)) {
      $thirdPartyTransID = $request->ThirdPartyTransID;
    }
    else {
      $thirdPartyTransID = 0;
    }
      $MSISDN = $request->MSISDN;
      $firstName = $request->FirstName;
      if(!empty($request->MiddleName)) {
      $middleName = $request->MiddleName;
    }
    else {
      $middleName = 0;
    }
    if(!empty($request->LastName)) {
      $lastName = $request->LastName;
    }
    else {
      $lastName = 0;
    }

      $checkAccount = Invoices::where('invoiceNo', $billRefNumber)->where('statusId', 0)->first();
      if (!$checkAccount) {
        $ResultCode = 1;
        $ResultDesc = "Rejected - AccountNumber not found";
        $message = "Dear " . strtoupper($firstName) . " " . strtoupper($middleName) . " " . strtoupper($lastName) . ", the account number provided does not exist in MOCAT. Thank you.";

        SMS::sendSMS($MSISDN, $message);
      } else if ($transAmount < 2) {
        $ResultCode = 1;
        $ResultDesc = "Rejected - Amount less than expected";
        $message = "Dear " . strtoupper($firstName) . " " . strtoupper($middleName) . " " . strtoupper($lastName) . ", the amount, Kshs. " . $transAmount . " is incorrect. Please check and try again.";

        SMS::sendSMS($MSISDN, $message);
      } else {
        $ResultCode = 0;
        $ResultDesc = "Accepted";
        c2b_payment::saveC2BTransaction($MSISDN, $firstName, $middleName, $lastName, $thirdPartyTransID, $orgAccountBalance, $invoiceNumber, $billRefNumber, $businessShortCode, $transAmount, $transTime, $transID, $transactionType);
      }

	  log::info("c2b validation---ResultCode-".$ResultCode."---".$ResultDesc);

      return array("ResultCode" => $ResultCode, "ResultDesc" => $ResultDesc);

  }

  public function C2Bconfirmation()
  {
    header('Access-Control-Allow-Origin: *');
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    Log::info("raw c2bconfirmation From MPESA---" . json_encode($request));

    if (isset($request->Body)) {
      Log::info("STK Confirmation MPESA---");//
      $transactionType = 0;
      $transID = 0;
      $transTime = 0;
      $transAmount = 0;
      $businessShortCode = 0;
      $billRefNumber = 0;
      $invoiceNumber = 0;
      $orgAccountBalance = 0;
      $thirdPartyTransID = 0;
      $MSISDN = 0;
      $firstName = 0;
      $middleName = 0;
      $lastName = 0;
    } else {
      $transactionType = $request->TransactionType;
      $transID = $request->TransID;
      $transTime = $request->TransTime;
      $transAmount = $request->TransAmount;
      $businessShortCode = $request->BusinessShortCode;
      $billRefNumber = $request->BillRefNumber;
      $invoiceNumber = $request->InvoiceNumber;
      $orgAccountBalance = $request->OrgAccountBalance;
      $thirdPartyTransID = $request->ThirdPartyTransID;
      $MSISDN = $request->MSISDN;
      $firstName = $request->FirstName;
      $middleName = $request->MiddleName;
      $lastName = $request->LastName;
    }

    c2b_payment::confirmC2BTransaction($transID,$billRefNumber);

    $res = Invoices::updateInvoice($billRefNumber);
    if($res) {
      return array("C2BPaymentConfirmationResult" => "Success");
    }
    else {
      return array("C2BPaymentConfirmationResult" => "Failed");
    }

  }

  public function requestSTKpush()
  {
    header('Access-Control-Allow-Origin: *');
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    Log::info("raw requestSTKpush MPESA---" . json_encode($request));

    $checkAccount = Invoices::where('invoiceNo', $request->invoiceNo)->where('statusId', 0)->first();
    if($checkAccount) {

    $amount = $checkAccount->cost;
    $phone = $request->mobileNo;

    $phone = "254" . substr($phone, -9);

	  $AccountReference = $request->invoiceNo;

    $TransactionDesc = $AccountReference;

    $BusinessShortCode = env("C2B_SHORTCODE");
    $LipaNaMpesaPasskey = env("LipaNaMpesaPasskey");
    $TransactionType = "CustomerPayBillOnline";
    $PartyB = env("C2B_SHORTCODE");
    $PartyA = $phone;
    $CallBackURL = env("C2B_CONFIRMATION_URL");
    $Remark = "MerveCabs Payment";

    $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
    $token = self::generateLiveToken();

    $timestamp = '20' . date("ymdhis");
    $password = base64_encode($BusinessShortCode . $LipaNaMpesaPasskey . $timestamp);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $token));
    $curl_post_data = array(
      'BusinessShortCode' => $BusinessShortCode,
      'Password' => $password,
      'Timestamp' => $timestamp,
      'TransactionType' => $TransactionType,
      'Amount' => $amount,
      'PartyA' => $PartyA,
      'PartyB' => $PartyB,
      'PhoneNumber' => $phone,
      'CallBackURL' => $CallBackURL,
      'AccountReference' => $AccountReference,
      'TransactionDesc' => $TransactionType,
      'Remark' => $Remark
    );
    $data_string = json_encode($curl_post_data);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($curl, CURLOPT_HEADER, false);
    $curl_response = curl_exec($curl);
    $result = json_decode($curl_response, true);
    curl_close($curl);

    //Error occurred
    if (isset($result['errorCode'])) {
      $errorCode = $result['errorCode'];
      $errorMessage = $result['errorMessage'];
      $CheckoutRequestID = 0;
      $MerchantRequestID = 0;
      $array = array("CheckoutRequestID" => $CheckoutRequestID, "MerchantRequestID" => $MerchantRequestID, "ResultCode" => 1, "ResultDesc" => "Sorry your request was not processed. Plese try again.");
      //return $this->asJson($array);

    } else {
      //return json_encode($curl_response);
      $CheckoutRequestID = $result['CheckoutRequestID'];
      $CustomerMessage = $result['CustomerMessage'];
      $MerchantRequestID = $result['MerchantRequestID'];
      $ResponseDescription = $result['ResponseDescription'];
      $ResponseCode = $result['ResponseCode'];
      $array = array("CheckoutRequestID" => $CheckoutRequestID, "MerchantRequestID" => $MerchantRequestID, "ResultCode" => $ResponseCode, "ResultDesc" => "Check your phone ( " . $PartyA . " ) and complete the transaction");
      //return $this->asJson($array);
    }

  }
  else {
    $array = array("CheckoutRequestID" => null, "MerchantRequestID" => null, "ResultCode" => null, "ResultDesc" => "Invoice number is already paid for");
    //return $this->asJson($array);
  }

  Log::info("raw STKPush-----" . json_encode($curl_response));
  return json_encode($array);

  }


}
