<?php
namespace App\Http\Controllers\customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\SMS;
use App\Otps;
use App\Groups;
use App\Grouptypes;
use App\Loantypes;
use App\Elections;
use App\Invites;
use App\Chats;
use App\Loans;
use App\Savings;
use App\Photos;
use App\Groupmembers;
use Auth;
use Log;
use App\Responseobject;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Image;
use Response;

class DashboardController extends Controller {

  public function uploadPhoto(Request $request)
  {

      $postdata = file_get_contents("php://input");
      $data = json_decode($postdata);
  	 // log::info("uploadPhoto---".json_encode($data));
  	  $response = new Responseobject;

  		if(isset($request->imageB64) && !empty($request->imageB64)) {
        $image = $request->imageB64;
  	    $timestamp = time();

        $imgdata = base64_decode($image);
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
        $temp=explode('/',$mime_type);
        $path = public_path("/photos/$timestamp.$temp[1]");
        file_put_contents($path,base64_decode($image));
        $photo = $timestamp.'.'.$temp[1];

        //compress
  		  $source = public_path().'/photos/'.$icon;
        $target = public_path().'/photos/'.$icon;
        Image::make($source)->fit(200, 200)->save($target);

        return Photos::uploadPhoto($request->userId,$photo);

  		}
  		else {
        $message = "Please upload a valid photo";
        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $message;
        $response->result = null;
        return Response::json($response);
  		}

  }

  public function uploadProfilePhoto(Request $request)
  {

      $postdata = file_get_contents("php://input");
      $data = json_decode($postdata);
  	 // log::info("uploadProfilePhoto---".json_encode($data));
  	  $response = new Responseobject;

  		if(isset($request->imageB64) && !empty($request->imageB64)) {
        $image = $request->imageB64;
  	    $timestamp = time();

        $imgdata = base64_decode($image);
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
        $temp=explode('/',$mime_type);
        $path = public_path("/photos/$timestamp.$temp[1]");
        file_put_contents($path,base64_decode($image));
        $photo = $timestamp.'.'.$temp[1];

        //compress
  		  $source = public_path().'/photos/'.$icon;
        $target = public_path().'/photos/'.$icon;
        Image::make($source)->fit(200, 200)->save($target);

        $res = User::where('id', $request->userId)->update(['profilePic'=>$photo]);
        if($res) {
          $response->status = $response::status_ok;
          $response->code = $response::code_ok;
          $response->message = "Successful";
          $response->result = User::getMainURL()."photos/".$photo;
        }
        else {
          $message = "Failed to upload profile photo";
          $response->status = $response::status_fail;
          $response->code = $response::code_fail;
          $response->message = $message;
          $response->result = null;
        }

  		}
  		else {
        $message = "Please upload a valid photo";
        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $message;
        $response->result = null;
  		}

      return Response::json($response);

  }

  public function removePhoto(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'id' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      $results = Photos::removePhoto($data->id);
      if($results) {
        $response->status = $response::status_ok;
        $response->code = $response::code_ok;
        $response->message = "Successful";
        $response->result = null;
    }
    else {
      $response->status = $response::status_fail;
      $response->code = $response::code_fail;
      $response->message = "Failed to remove photo, please try again";
    }

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function groupTypes(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      $results = Grouptypes::groupTypes($data->userId);
      if($results) {
        $response->status = $response::status_ok;
        $response->code = $response::code_ok;
        $response->message = "Successful";
        $response->result = $results;
    }
    else {
      $response->status = $response::status_fail;
      $response->code = $response::code_fail;
      $response->message = "Failed";
    }

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function joinGroup(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'groupId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      return Groupmembers::joinGroup($data->userId,$data->groupId);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }


  public function groupDetails(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'groupId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      $res = Groups::groupDetails($data->userId,$data->groupId);
      $response->status = $response::status_ok;
      $response->code = $response::code_ok;
      $response->message = "Successful";
      $response->result = $res;

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function mySavings(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      $res = Savings::mySavings($data->userId);
      $response->status = $response::status_ok;
      $response->code = $response::code_ok;
      $response->message = "Successful";
      $response->result = $res;

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function myPhotos(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      $res = Photos::myPhotos($data->userId);
      $response->status = $response::status_ok;
      $response->code = $response::code_ok;
      $response->message = "Successful";
      $response->result = $res;

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function exitGroup(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'groupId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      return Groupmembers::exitGroup($data->userId,$data->groupId);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function createGroup(Request $request)
  {

      $postdata = file_get_contents("php://input");
      $request = json_decode($postdata);
  	 log::info("createGroup---".json_encode($request));
  	  $response = new Responseobject;

  		if(isset($request->imageB64) && !empty($request->imageB64)) {
        $image = $request->imageB64;
  	    $timestamp = time();

        $imgdata = base64_decode($image);
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
        $temp=explode('/',$mime_type);
        $path = public_path("/photos/$timestamp.$temp[1]");
        file_put_contents($path,base64_decode($image));
        $photo = $timestamp.'.'.$temp[1];

        //compress
  		  $source = public_path().'/photos/'.$icon;
        $target = public_path().'/photos/'.$icon;
        Image::make($source)->fit(200, 200)->save($target);

      }
      else {
        $photo = "profile.jpg";
      }

        return Groups::createGroup($request->userId,$request->groupName,$request->groupTypeId,$request->description,$photo);

  }

  public function editGroup(Request $request)
  {

      $postdata = file_get_contents("php://input");
      $data = json_decode($postdata);
  	 // log::info("editGroup---".json_encode($data));
  	  $response = new Responseobject;

  		if(isset($request->imageB64) && !empty($request->imageB64))
      {
        $image = $request->imageB64;
  	    $timestamp = time();

        $imgdata = base64_decode($image);
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);
        $temp=explode('/',$mime_type);
        $path = public_path("/photos/$timestamp.$temp[1]");
        file_put_contents($path,base64_decode($image));
        $photo = $timestamp.'.'.$temp[1];

        //compress
  		  $source = public_path().'/photos/'.$icon;
        $target = public_path().'/photos/'.$icon;
        Image::make($source)->fit(200, 200)->save($target);

  		}
  		else
      {
        $gp = Groups::where('id',$request->groupId)->first();
        $photo = $gp->groupIcon;
  		}

      return Groups::editGroup($request->userId,$request->groupId,$request->groupName,$photo);

  }

  public function groups(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      $res = Groups::groups($data->userId);
      $response->status = $response::status_ok;
      $response->code = $response::code_ok;
      $response->message = "Successful";
      $response->result = $res;

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function myGroups(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      $res = Groups::myGroups($data->userId);
      $response->status = $response::status_ok;
      $response->code = $response::code_ok;
      $response->message = "Successful";
      $response->result = $res;

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }


  public function groupMembers(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'groupId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      return Groupmembers::groupMembers($data->userId,$data->groupId);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function myProfile(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      $res = User::myProfile($data->userId);
      $response->status = $response::status_ok;
      $response->code = $response::code_ok;
      $response->message = "Successful";
      $response->result = $res;

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function loanTypes(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      return Loantypes::loanTypes($data->userId);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function sendInvite(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'mobileNo' => 'bail|required|integer',
        'message' => 'bail|required|string',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {
      $mobileNo = "254".substr($data->mobileNo, -9);
      return SMS::sendInviteSMS($mobileNo,$data->message);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function applyLoan(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'loanTypeId' => 'bail|required|integer',
        'groupId' => 'bail|required|integer',
        'amount' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      return Loans::applyLoan($data->userId,$data->loanTypeId,$data->groupId,$data->amount);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function myLoans(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      $res = Loans::myLoans($data->userId);
      $response->status = $response::status_ok;
      $response->code = $response::code_ok;
      $response->message = "Successful";
      $response->result = $res;

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function groupChats(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'groupId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      return Chats::groupChats($data->userId,$data->groupId);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function postChat(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'groupId' => 'bail|required|integer',
        'message' => 'bail|required|string|max:255|min:1',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      return Chats::postChat($data->userId,$data->groupId,$data->message);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function elect(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'groupId' => 'bail|required|integer',
        'adminId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      return Elections::elect($data->userId,$data->groupId,$data->adminId);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function election(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'groupId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      return Elections::election($data->userId,$data->groupId);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function invite(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'groupId' => 'bail|required|integer',
        'mobileNo' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      $mobileNo = "254".substr($data->mobileNo, -9);

      return Invites::invite($data->userId,$data->groupId,$mobileNo);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function myInvites(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      return Invites::myInvites($data->userId);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function actOnInvite(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'id' => 'bail|required|integer',
        'status' => 'bail|required|string',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      return Invites::actOnInvite($data->id,$data->status);

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }


}
