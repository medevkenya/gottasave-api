<?php
namespace App\Http\Controllers\customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\SMS;
use App\Otps;
use Auth;
use Log;
use App\Responseobject;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Response;

class AccountController extends Controller {

  /**
  * Phone number verification
  */
  public function requestOtp()
  {
    header('Access-Control-Allow-Origin: *');

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'mobileNo' => 'bail|required|digits:12|unique:users',
        'fcmToken' => 'bail|required|string',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

      if(User::checkAccess($data->hashedKey)) {

      return User::requestOtp($data->hashedKey,$data->mobileNo,$data->fcmToken);

    }
    else {
      $message = "Invalid authentication";
      $response->status = $response::status_fail;
      $response->code = $response::code_fail;
      $response->message = $message;
      $response->result = null;
    }

    }

    return Response::json($response);

  }

  public function verifyOtp(Request $request)
  {

    $response = new Responseobject;
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
log::info(json_encode($data));
    $otpCode = $data->otpCode;

    $mobileNo = "254".substr($data->mobileNo, -9);

    if(User::checkAccess($data->hashedKey)) {

      if(User::getUserBymobileNo($mobileNo)) {

      $res = Otps::validateOtp($mobileNo,$otpCode);
      if($res) {
        $response->status = $response::status_ok;
        $response->code = $response::code_ok;
        $response->message = "Your mobile no. was successfully verified. Please login.";
        $response->result = User::getUserBymobileNo($mobileNo);
      }
      else {
        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = "Failed to veriy. Invalid OTP code";
      }

    }
    else {
      $response->status = $response::status_not_found;
      $response->code = $response::code_not_found;
      $response->message = "Mobile No. is not registered";
    }

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

    return Response::json($response);

  }


  public function login(Request $request)
  {

   $data = json_decode(file_get_contents('php://input'));
   $response = new Responseobject;
   $array_data = (array)$data;

   $validator = Validator::make($array_data, [
       'hashedKey' => 'bail|required|string|max:255',
       'mobileNo' => 'bail|required'
   ]);

   if($validator->fails()){

       $response->status = $response::status_fail;
       $response->code = $response::code_fail;
       $response->message = $validator->messages()->first();
       foreach ($validator->errors()->getMessages() as $item) {
           array_push($response->messages, $item);
       }

   }
   else{


    if(User::checkAccess($data->hashedKey)) {

      $mobileNo = "254".substr($data->mobileNo, -9);

      if(User::getUserBymobileNo($mobileNo)) {

        $checkUser = User::where('mobileNo',$mobileNo)->first();
        if($checkUser->isDisabled == 1) {
          $response->status = $response::status_fail;
          $response->code = $response::code_fail;
          $response->message = "Your account is disabled. Please contact admin on ".$adminContact." for help";
          $response->result = null;
        }
        else if($checkUser->isVerified == 0) {
          $response->status = $response::status_ok;
          $response->code = $response::code_ok;
          $response->message = "Your account is not verified. We have sent an SMS with verification code to ".$mobileNo."";
          $response->result = User::getUserBymobileNo($mobileNo);
          Otps::requestOtp($mobileNo);
        }
        else {

      $results = Otps::requestOtp($mobileNo);//User::loginUser($mobileNo);
      if($results) {
        $response->status = $response::status_ok;
        $response->code = $response::code_ok;
        $response->message = "Successful login";
        $response->result = User::getUserBymobileNo($mobileNo);
      }
      else {
        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = "Failed login. Wrong mobile no. or password";
      }

    }

    }
    else {
      $response->status = $response::status_not_found;
      $response->code = "Reg";
      $response->message = "Mobile No. is not registered";
    }

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function forgotPass(Request $request)
  {

     $response = new Responseobject;

    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);

    if(User::checkAccess($data->hashedKey)) {

      $mobileNo = "254".substr($data->mobileNo, -9);

      if(User::getUserBymobileNo($mobileNo)) {

        $checkUser = User::where('mobileNo',$mobileNo)->first();
        if($checkUser->isDisabled == 1) {
          $response->status = $response::status_fail;
          $response->code = $response::code_fail;
          $response->message = "Your account is disabled. Please contact admin on ".$adminContact." for help";
          $response->result = null;
        }
        else if($checkUser->isVerified == 0) {
          $response->status = $response::status_fail;
          $response->code = 2;//$response::code_fail;
          $response->message = "Your account is not verified. We have sent an SMS with verification code to ".$mobileNo."";
          $response->result = null;
          Otps::requestOtp($mobileNo);
        }
        else {

      $results = User::forgotpass($mobileNo);
      if($results) {
        $response->status = $response::status_ok;
        $response->code = $response::code_ok;
        $response->message = "Successful. We have sent an SMS with a one time password. Use it to login and your password to a new one";
        $response->result = null;
      }
      else {
        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = "Failed reset password. Wrong mobile number";
      }

    }

    }
    else {
      $response->status = $response::status_not_found;
      $response->code = $response::code_not_found;
      $response->message = "Mobile No. is not registered";
    }

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

    return Response::json($response);

  }


  public function register(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'firstName' => 'bail|required|string|max:255',
        'lastName' => 'bail|required|string|max:255',
        'mobileNo' => 'bail|required|digits:10',
        'nationalId' => 'bail|integer|nullable',
        'gender' => 'bail|required|string',
        'kraPin' => 'bail|string|nullable',
        'funny' => 'bail|string|nullable',
        'hobby' => 'bail|string|nullable',
        'dob' => 'bail|required',
        'wardId' => 'bail|required|integer',
        'countyId' => 'bail|required|integer',
        'password' => 'bail|required|string',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

    $mobileNo = "254".substr($data->mobileNo, -9);

    $checkU =  User::where('mobileNo',$mobileNo)
    //->where('isVerified',1)
    ->where('isDeleted',0)->first();

      if(!$checkU) {

      $results = User::registerUser($mobileNo,$data->firstName,$data->lastName,$data->dob,$data->gender,$data->countyId,$data->wardId,$data->kraPin,$data->nationalId,$data->hobby,$data->funny,$data->password);
      if($results) {
        $response->status = $response::status_ok;
        $response->code = $response::code_ok;
        $response->message = "Successful registration, login to proceed.";
        $response->result = User::getUserBymobileNo($mobileNo);
      }
      else {
        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = "Failed registration. Please try again";
      }
    }
    else {
      $response->status = $response::status_fail;
      $response->code = $response::code_fail;
      $response->message = "Mobile number already exists";
    }

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }

  public function changePassword(Request $request)
  {

    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);

    log::info("changePassword--".json_encode($data));

    return User::changePassword($data->userId,$data->newPassword,$data->currentPassword);

  }

  public function editProfile(Request $request)
  {

    $data = json_decode(file_get_contents('php://input'));
    $response = new Responseobject;
    $array_data = (array)$data;

    $validator = Validator::make($array_data, [
        'hashedKey' => 'bail|required|string|max:255',
        'firstName' => 'bail|required|string|max:255',
        'lastName' => 'bail|required|string|max:255',
        'userId' => 'bail|required|integer',
        'nationalId' => 'bail|required|integer',
        'gender' => 'bail|required|string',
        'kraPin' => 'bail|required|string',
        'funny' => 'bail|required|string',
        'hobby' => 'bail|required|string',
        'dob' => 'bail|required',
        'wardId' => 'bail|required|integer',
        'countyId' => 'bail|required|integer',
    ]);

    if($validator->fails()){

        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $validator->messages()->first();
        foreach ($validator->errors()->getMessages() as $item) {
            array_push($response->messages, $item);
        }

    }
    else {

    if(User::checkAccess($data->hashedKey)) {

      return User::editProfile($data->userId,$data->firstName,$data->lastName,$data->dob,$data->gender,$data->countyId,$data->wardId,$data->kraPin,$data->nationalId,$data->hobby,$data->funny);
      // if($results) {
      //   $response->status = $response::status_ok;
      //   $response->code = $response::code_ok;
      //   $response->message = "Your profile was updated successfully";
      //   $response->result = User::getUserBymobileNo($mobileNo);
      // }
      // else {
      //   $response->status = $response::status_fail;
      //   $response->code = $response::code_fail;
      //   $response->message = "Failed to save changes. Please try again";
      // }

    }
    else {
      $response->status = $response::status_unauthorized;
      $response->code = $response::code_unauthorized;
    }

  }

    return Response::json($response);

  }


}
