<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Notifications;
use Auth;
use Log;
use App\mobile\Responseobject;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Response;

class NotificationsController extends Controller {

  public function getNotifications($userId,$pageNumber)
  {
    return Notifications::getNotifications($userId,$pageNumber);
  }

}
