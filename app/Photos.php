<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use App\Responseobject;
use Response;
use Log;

class Photos extends Model
{
    protected $table = 'photos';

    public static function myPhotos($userId) {
      $data = Photos::where('userId',$userId)->where('isDeleted',0)->limit(4)->get();
      $all = array();
      foreach ($data as $key) {
        $photo = User::getMainURL()."photos/".$key->photo;
        $all[] = array('id'=>$key->id,'photo'=>$photo);
      }
      return $all;
    }

    public static function uploadPhoto($userId,$photo) {
      $model = new Photos;
      $model->userId = $userId;
      $model->photo = $photo;
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

    public static function removePhoto($photoId) {
      $model = Photos::find($photoId);
      $model->isDeleted = 1;
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

}
