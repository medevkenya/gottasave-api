<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use App\Responseobject;
use Response;
use Log;

class Loans extends Model
{
    protected $table = 'loans';

    public static function applyLoan($userId,$loanTypeId,$groupId,$amount) {
      $response = new Responseobject;

        $checkuser = Loans::where('userId',$userId)->where('status',"active")->where('isDeleted',0)->first();
        if(!$checkuser) {
          $message = "You already have an active loan";
          $response->status = $response::status_fail;
          $response->code = $response::code_fail;
          $response->message = $message;
          $response->result = null;
        }
        else
        {
          $model = new Loans;
          $model->userId = $userId;
          $model->groupId = $groupId;
          $model->loanTypeId = $loanTypeId;
          $model->amount = $amount;
          $model->applicationDate = date('Y-m-d');
          $model->save();
          if ($model) {
            $response->status = $response::status_ok;
            $response->code = $response::code_ok;
            $response->message = "You have successfully created a new group";
            $response->result = null;
          }
          else {
            $message = "Failed to create group, try again";
            $response->status = $response::status_fail;
            $response->code = $response::code_fail;
            $response->message = $message;
            $response->result = null;
        }
        }
        return Response::json($response);
    }

    public static function myLoans($userId) {
      $data = Loans::select('loans.*','groups.groupName','groups.groupIcon')
      ->leftJoin('groups','loans.groupId','=','groups.id')
      ->where('loans.userId',$userId)
      ->where('loans.isDeleted',0)->get();
      $all = array();
      foreach ($data as $key) {
        $groupIcon = User::getMainURL()."photos/".$key->groupIcon;
        $status = strtoupper($key->status);
        $all[] = array('id'=>$key->id,'amount'=>$key->amount,'status'=>$status,'applicationDate'=>$key->applicationDate,'repaymentDate'=>$key->repaymentDate,'approvalDate'=>$key->approvalDate,'groupName'=>$key->groupName,'groupIcon'=>$groupIcon);
      }
      return $all;
    }


}
