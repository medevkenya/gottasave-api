<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use App\Responseobject;
use Response;
use Log;

class Chats extends Model
{
    protected $table = 'chats';

    public static function postChat($userId,$groupId,$message) {
      $response = new Responseobject;

        $checkuser = Chats::where('userId',$userId)->where('groupId',$groupId)->where('message',$message)->where('isDeleted',0)->first();
        if(!$checkuser) {
          $message = "Message already posted";
          $response->status = $response::status_fail;
          $response->code = $response::code_fail;
          $response->message = $message;
          $response->result = null;
        }
        else
        {
          $model = new Chats;
          $model->userId = $userId;
          $model->groupId = $groupId;
          $model->message = $message;
          $model->save();
          if ($model) {
            $response->status = $response::status_ok;
            $response->code = $response::code_ok;
            $response->message = "Posted successfully";
            $response->result = null;
          }
          else {
            $message = "Failed to post, try again";
            $response->status = $response::status_fail;
            $response->code = $response::code_fail;
            $response->message = $message;
            $response->result = null;
        }
        }
        return Response::json($response);
    }

    public static function groupChats($userId) {
      $data = Chats::select('chats.*','users.firstName','users.lastName','users.profilePic')
      ->leftJoin('users','chats.userId','=','users.id')
      ->where('chats.userId',$userId)
      ->where('chats.isDeleted',0)->get();
      $all = array();
      foreach ($data as $key) {
        $profilePic = User::getMainURL()."photos/".$key->profilePic;
        $name = $key->firstName." ".$key->lastName;
        $date = self::convertDate($key->created_at);
        $all[] = array('id'=>$key->id,'name'=>$name,'date'=>$date,'message'=>$key->message);
      }
      return $all;
    }

    public static function convertDate($date) {
      return date("d-m-Y H:i A", strtotime($date));
    }


}
