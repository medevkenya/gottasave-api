<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use Response;
use Log;

class Notifications extends Model
{
    protected $table = 'notifications';

    public static function getInsights() {
      return array();
    }

    public static function getNotifications($userId,$pageNumber) {
      //update all to read
    Notifications::where('userId',$userId)->update(['isRead' => 1]);
    $sarray = array();

    $per_page = 10;
    $list1 = Notifications::where('userId',$userId)->where('isDeleted',0)->orderBy('id','DESC')->paginate($per_page, ['*'], 'page', $pageNumber);

    $total_count = Notifications::where('userId',$userId)->where('isDeleted',0)->count();
    $total_pages = ceil($total_count / $per_page);
    foreach ($list1 as $listitem) {
    $sarray[] = array('id'=>$listitem->id,'notification' =>$listitem->notification,'created_at'=>date("Y-m-d H:i", strtotime($listitem->created_at)),'isRead'=>$listitem->isRead);
    }

    return array('total' =>$total_count,'per_page'=>$per_page,'page'=>$pageNumber,'total_pages'=>$total_pages,'data'=>$sarray);

}

public static function getNotificationCount($userId) {
  return Notifications::where('userId',$userId)->where('isRead',0)->count();
}

public static function CreateNotification($userId,$notification) {
  $model = new Notifications;
  $model->notification = $notification;
  $model->userId = $userId;
  $model->save();
}

/**
* flexyfcm
*/
public static function SendFCMMessage($customerId,$messageContent)
{
  self::CreateNotification($customerId,$messageContent);
// header('Access-Control-Allow-Origin: *');
//
// log::info('fcmDRIVER-1-'.$customerId);
//
// $getuser = User::where('id', $customerId)->first();
// if($getuser) {
//
// $fcmToken = $getuser->fcmToken;
//
// log::info('fcmDRIVER-2-'.$fcmToken);
//
// $messageTitle = "Trip";
// $methodName = null;
// $dataArray = null;
//
// // API access key from Google API's Console
// // define('API_ACCESS_KEY','YOUR-API-ACCESS-KEY-GOES-HERE');//
//
//   //if($accountType == 2) {
//   $API_ACCESS_KEY = "ch_L8dNy4sA:APA91bFIlpqWz3fR5vfeplbXdc4e6pJDPkquGiGVwtB0HzL-jn21YH7pWu02gn9n8pFfr-yn4T6YSrDBjzRipNhhscpNNxn0bh0NDSld80S75mXiZrvEZIrA6j6IWq6_ujB91xFw_59M";
// // }
// // else {
// //   $API_ACCESS_KEY = "AAAA7Jh1pdk:APA91bEIjXwa6GnUvicY9jF5nDfhp8Pu7DatZHKg0j8hRTNwTZCJWQq9eupqv_Vq_x6kkpmOZQCthqnJtuuul3VGyGernbzRpec2h4R3SJkCalW9BKdgUb6vb_Ql9tUT5eQ1Hi_Dmc2s";
// // }
//
// $url = 'https://fcm.googleapis.com/fcm/send';
//   //$registrationIds = array($_GET['id']);
//   // prepare the message
//   // $message = array(
//    // 'title'     => $messageTitle,//'Flexy Title.',
//    // 'body'      => $messageContent,//'Here is a message from Cabs.',
//    // 'vibrate'   => 1,
//    // 'sound'     => 1
//   // );
//
//   // $fields = array(
//    // 'registration_ids' => $registrationIds,
//    // 'data'             => $message
//   // );
//   //"http://www.androiddeft.com/wp-content/uploads/2017/11/Shared-Preferences-in-Android.png"
//
//   $fields = array(
//         'to'=>$fcmToken,
//         'priority'=>'high',
//         "mutable_content"=>true,
//         "notification"=>array(
//                     "title"=> $messageTitle,
//                     "body"=> $messageContent,
//                     "vibrate"   => 1,
//                   "sound"     => 1,
//                   //"image" => "https://flexycabs.co.ke/public/assets/images/logo.PNG"
//         ),
//         'data'=>$dataArray
//       );
//
//   $headers = array(
//     'Authorization: key='.$API_ACCESS_KEY,
//     'Content-Type: application/json'
//   );
//   $ch = curl_init();
//   curl_setopt( $ch,CURLOPT_URL,$url);
//   curl_setopt( $ch,CURLOPT_POST,true);
//   curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
//   curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
//   curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
//   curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields));
//   $result = curl_exec($ch);
//   curl_close($ch);
//
// log::info('fcmDRIVER-3-'.$result);
//
//   $cur_message=json_decode($result);
//       // if($cur_message->success==1) {
//       // self::saveFcmMessage($userId,$messageTitle,$messageContent,$methodName,$dataArray,1);
//       // //return true;
//       // }
//       // else {
//       // self::saveFcmMessage($userId,$messageTitle,$messageContent,$methodName,$dataArray,0);
//       // //return false;
//       // }
// }
}

}
