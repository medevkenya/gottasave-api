<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use App\Responseobject;
use Response;
use Log;

class Invites extends Model
{
    protected $table = 'invites';

    public static function invite($userId,$groupId,$mobileNo) {
      $response = new Responseobject;

      $user = User::getUserById($userId);

      if($mobileNo == $user->mobileNo) {
        $message = "You cannot invite yourself";
        $response->status = $response::status_fail;
        $response->code = $response::code_fail;
        $response->message = $message;
        $response->result = null;
      }
      else {

        $checkuser = Invites::where('userId',$userId)->where('groupId',$groupId)
        ->where('mobileNo',$mobileNo)
        ->where('isDeleted',0)->first();
        if(!$checkuser) {
          $message = "You have already invited ".$mobileNo;
          $response->status = $response::status_fail;
          $response->code = $response::code_fail;
          $response->message = $message;
          $response->result = null;
        }
        else
        {
          $model = new Invites;
          $model->userId = $userId;
          $model->groupId = $groupId;
          $model->mobileNo = $mobileNo;
          $model->save();
          if ($model) {
            $response->status = $response::status_ok;
            $response->code = $response::code_ok;
            $response->message = "Invited successfully, SMS sent to ".$mobileNo;
            $response->result = null;
          }
          else {
            $message = "Failed to invite, try again";
            $response->status = $response::status_fail;
            $response->code = $response::code_fail;
            $response->message = $message;
            $response->result = null;
        }
        }
      }
        return Response::json($response);
    }

    public static function myInvites($userId) {
      $user = User::getUserById($userId);
      $data = Invites::select('invites.*','groups.groupName','users.firstName','users.lastName','users.profilePic')
      ->leftJoin('users','invites.userId','=','users.id')
      ->leftJoin('groups','invites.groupId','=','groups.id')
      ->where('invites.mobileNo',$user->mobileNo)
      ->where('invites.isDeleted',0)->get();
      $all = array();
      foreach ($data as $key) {
        $profilePic = User::getMainURL()."photos/".$key->profilePic;
        $name = $key->firstName." ".$key->lastName;
        $date = self::convertDate($key->created_at);
        $status = strtoupper($key->status);
        $all[] = array('id'=>$key->id,'status'=>$status,'name'=>$name,'groupName'=>$groupName,'date'=>$date,'profilePic'=>$profilePic);
      }
      return $all;
    }

    public static function convertDate($date) {
      return date("d-m-Y H:i A", strtotime($date));
    }

    public static function actOnInvite($id,$status) {
          $response = new Responseobject;

          $model = Invites::where('id', $id)->update(['status'=>$status]);
          if ($model) {
            $response->status = $response::status_ok;
            $response->code = $response::code_ok;
            $response->message = "You have successfully ".$status." the invite";
            $response->result = null;
          }
          else {
            $message = "Failed, try again";
            $response->status = $response::status_fail;
            $response->code = $response::code_fail;
            $response->message = $message;
            $response->result = null;
        }

        return Response::json($response);
    }


}
