<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use App\Responseobject;
use Response;
use AfricasTalking\SDK\AfricasTalking;

class SMS extends Model
{
    protected $table = 'sms';

    public static function sendInviteSMS($mobileNo,$message) {
      $response = new Responseobject;
      $response->status = $response::status_ok;
      $response->code = $response::code_ok;
      $response->message = "Your invite was sent successfully";
      $response->result = null;
      return Response::json($response);
    }

/**
* Save record
*/
public static function sendSMS($mobileNo,$message) {

    $userDetails = User::getUserBymobileNo($mobileNo);
    if($userDetails) {
      $userId      = $userDetails['id'];
    }
    else {
      $userId = 0;
    }

    $model = new SMS;
    $model->userId = $userId;
    $model->mobileNo = $mobileNo;
    $model->message = $message;
    $model->save();
    if($model) {

    // $new = str_replace(' ', '%20', $message);//

    //self::sendsmsnow($mobileNo,$message);

      return true;
    }

    return false;

}

  /**
  * Send sms now
  */
  public static function sendsmsnow($mobileNo,$message) {

  // Specify your login credentials
  $username   = "mervecabs";
  $apiKey     = "a646220c8d95cd5eea47aa577c5de330b8ace70bbf3e9e16bff7091cc0b42925";
  $senderid = "MERVE-CABS";

  $AT       = new AfricasTalking($username, $apiKey);

  // Get one of the services
  $sms      = $AT->sms();

  // Use the service
  $result   = $sms->send([
    'from'      => $senderid,
    'to'        => $mobileNo,
    'message'   => $message
  ]);

  return true;
  //print_r($result);

  }

  public static function CronSendCounty()
{
  date_default_timezone_set("Africa/Nairobi");
  ini_set('max_execution_time', 999999999);
    //Specify your login credentials
	$username   = env("SMS_Username");
  $apikey     = env("SMS_Key");

  log::info("CronSendCounty--".date('Y-m-d H:i:s')."--");

	$date= date('Y-m-d');
	$time = date('H:i');
	$checkbatch = DB::table('ref_queue')->where('overall_status','0')->where('send_date','=',$date)->where('send_time','<=',$time)->orderBy('id','DESC')->first();
	if(!$checkbatch) { exit(); }
	$ref_queue_id = $checkbatch->ref_queue_id;
	$senderid = $checkbatch->senderid;
	$message = $checkbatch->message;

	$batch_limit_size = 1000;
    $maincustomers = DB::table('messages')->where('ref_queue_id', $ref_queue_id)->where('status','0')->limit($batch_limit_size)->get();

	$lines = array();
	$count_processed = 0;
	foreach( $maincustomers as $customer ) {
	$phone = $customer->phone;
	$newphone =  substr($phone, -9);
	$phone = "254".$newphone;
	$lines[] = $phone;
	$count_processed++;

	//DB::insert('insert into messages_sent (phone,ref_queue_id,status,message) values (?,?,?,?)', [$newphone,$ref_queue_id,1,$message]);
	DB::table('messages')->where('ref_queue_id', $ref_queue_id)->where('phone',$newphone)->delete();

	}

	if(count($lines)>1){
		$recipients = implode(',', $lines);
	}else{
		$recipients =$lines[0];
	}

	$gateway    = new AfricasTalkingGateway($username, $apikey);

	try
	{

	$results = $gateway->sendMessage($recipients, $message,$senderid);

	foreach($results as $result) {

	$msglen=strlen($message);
	$multipl = ceil($msglen/160);
	$costt = $multipl * 1;

	$phone = $result->number;
	$phone =  substr($phone, -9);
$status = $result->status;
$messageid = $result->messageId;
//echo " Cost: "   .$result->cost."\n";

$getbatch = DB::table('ref_queue')->where('id', $ref_queue_id)->first();

//if($status == "Success" || $status == "Sent") {
	//DB::table('messages')->where('phone', $phone)->where('ref_queue_id', $ref_queue_id)->update(['status' => 1]);
	$addsent = DB::insert('insert into messages_sent (mobileNo,ref_queue_id) values (?,?)', [$phone,$ref_queue_id]);
	DB::table('messages')->where('ref_queue_id', $ref_queue_id)->where('phone',$phone)->delete();
	$batch_sent = $getbatch->batch_sent + 1;
	DB::table('ref_queue')->where('id', $ref_queue_id)->update(['batch_sent' => $batch_sent]);
//}
//else {
	//DB::table('messages')->where('phone', $phone)->where('ref_queue_id', $ref_queue_id)->update(['status' => 3]);
	//DB::table('ref_queue')->where('phone', $phone)->where('ref_queue_id', $ref_queue_id)->update(['status' => 1]);
//}

	}

	$getbatch = DB::table('ref_queue')->where('id', $ref_queue_id)->first();
	$batch_processed = $getbatch->batch_processed + $count_processed;
	$batch_size= $getbatch->batch_size;
	DB::table('ref_queue')->where('id', $ref_queue_id)->update(['batch_processed' => $batch_processed]);

	if($batch_processed >= $batch_size) {
		DB::table('ref_queue')->where('id', $ref_queue_id)->update(['overall_status' => 1]);
	}

	}
	catch ( AfricasTalkingGatewayException $e ) {
		echo "Encountered an error while sending: ".$e->getMessage();
	}
	// DONE!!!

}

}
