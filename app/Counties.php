<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Counties extends Model
{

    protected $table = 'counties';

    public static function GetAll() {
      return Counties::select('countyName','id')->where('isDeleted',0)->get();
    }

}
