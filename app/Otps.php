<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Hash;
use App\Responseobject;
use Response;
use App\User;
use App\SMS;
use Illuminate\Support\Facades\Log;

class Otps extends Model
{

  protected $table = 'otps';

  /**
  * Save record
  */
  public static function requestOtp($mobileNo) {
    $otp = mt_rand(1000,9999);
    $password = Hash::make($otp);

    SMS::sendSMS($mobileNo,"Your One Time Pin is : ".$otp);

    $model = new Otps;
    $model->mobileNo = $mobileNo;
    $model->otpCode = $otp;
    $model->save();
    if($model) {
      return $otp;
    }
    else {
      return false;
    }

  }


  public static function validateOtp($mobileNo,$otp) {
$response = new Responseobject;
    //$userId = $userDetails->id;
    $mobileNo = "254".substr($mobileNo, -9);
    $checkuser = Otps::where('mobileNo', $mobileNo)->where('otpCode', $otp)
    ->where('status', 1)->where('isDeleted', 0)->first();

    if ($checkuser)
    {

      $message = "Successful OTP verification";
      Otps::where('mobileNo', $mobileNo)->where('otpCode', $otp)->where('isDeleted', 0)->update(['status' => 0]);
      $response->status = $response::status_fail;
      $response->code = $response::code_fail;
      $response->message = $message;
      $response->result = null;

      $userDetails = User::getUserBymobileNo($mobileNo);
      $userId      = $userDetails['id'];
      User::where('id',$userId)->update(['isVerified' => 1]);

    }
    else {

      $message = "Invalid One Time Password. Check the code and try again";
      $response->status = $response::status_fail;
      $response->code = $response::code_fail;
      $response->message = $message;
      $response->result = null;

    }
return Response::json($response);
  }

}
