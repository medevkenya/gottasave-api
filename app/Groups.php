<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use App\Responseobject;
use Response;
use Log;
use App\Groupmembers;
use App\Savings;

class Groups extends Model
{
    protected $table = 'groups';

    public static function createGroup($userId,$groupName,$groupTypeId,$description,$groupIcon) {
      $response = new Responseobject;

        $checkuser = Groups::where('groupName',$groupName)->where('groupTypeId',$groupTypeId)->where('isDeleted',0)->first();
        if($checkuser) {
          $message = "Name is already taken, use another one";
          $response->status = $response::status_fail;
          $response->code = $response::code_fail;
          $response->message = $message;
          $response->result = null;
        }
        else
        {
          $model = new Groups;
          $model->adminId = $userId;
          $model->groupName = $groupName;
          $model->groupTypeId = $groupTypeId;
          $model->groupIcon = $groupIcon;
          $model->description = $description;
          $model->save();
          if ($model) {
            Groupmembers::joinGroup($userId,$model->id);
            $response->status = $response::status_ok;
            $response->code = $response::code_ok;
            $response->message = "You have successfully created a new group";
            $response->result = null;
          }
          else {
            $message = "Failed to create group, try again";
            $response->status = $response::status_fail;
            $response->code = $response::code_fail;
            $response->message = $message;
            $response->result = null;
        }
        }
        return Response::json($response);
    }

    public static function editGroup($userId,$groupId,$groupName,$groupIcon) {
          $response = new Responseobject;
          $model = Groups::find($groupId);
          $model->groupName = $groupName;
          $model->groupIcon = $groupIcon;
          $model->save();
          if ($model) {
            $response->status = $response::status_ok;
            $response->code = $response::code_ok;
            $response->message = "You have successfully edited group";
            $response->result = null;
          }
          else {
            $message = "Failed to edit group, try again";
            $response->status = $response::status_fail;
            $response->code = $response::code_fail;
            $response->message = $message;
            $response->result = null;
        }
        return Response::json($response);
    }

    public static function groups($userId) {
      $data = Groups::select('groups.*','grouptypes.groupTypeName','grouptypes.capacity','users.firstName','users.lastName')
      ->leftJoin('grouptypes','groups.groupTypeId','=','grouptypes.id')
      ->leftJoin('users','groups.adminId','=','users.id')
      ->where('groups.isDeleted',0)->get();
      $all = array();
      foreach ($data as $key) {
        $groupIcon = User::getMainURL()."photos/".$key->groupIcon;
        $adminName = $key->firstName." ".$key->lastName;
        $countMembers = Groupmembers::where('groupId',$key->id)->where('isDeleted',0)->count();
        $all[] = array('id'=>$key->id,'groupName'=>$key->groupName,'groupIcon'=>$groupIcon,'description'=>$key->description,'status'=>$key->status,'groupTypeId'=>$key->groupTypeId,'groupTypeName'=>$key->groupTypeName,'capacity'=>$key->capacity,'adminName'=>$adminName,'countMembers'=>$countMembers);
      }
      return $all;
    }

    public static function myGroups($userId) {
      $data = Groupmembers::select('groupmembers.*','groups.groupName','groups.groupIcon','grouptypes.groupTypeName','grouptypes.capacity','users.firstName','users.lastName')
      ->leftJoin('groups','groupmembers.groupId','=','groups.id')
      ->leftJoin('grouptypes','groups.groupTypeId','=','grouptypes.id')
      ->leftJoin('users','groups.adminId','=','users.id')
      ->where('groupmembers.userId',$userId)
      ->where('groupmembers.isDeleted',0)
      ->groupBy('groupmembers.groupId')->get();
      $all = null;
      foreach ($data as $key) {
        $groupIcon = User::getMainURL()."photos/".$key->groupIcon;
        $adminName = $key->firstName." ".$key->lastName;
        $countMembers = Groupmembers::where('groupId',$key->groupId)->where('isDeleted',0)->count();
        $countComments = Chats::where('groupId',$key->id)->where('isDeleted',0)->count();
        $all[] = array('id'=>$key->id,'groupName'=>$key->groupName,'groupIcon'=>$groupIcon,'description'=>$key->description,'status'=>$key->status,'groupTypeId'=>$key->groupTypeId,'groupTypeName'=>$key->groupTypeName,'capacity'=>$key->capacity,'adminName'=>$adminName,'countComments'=>$countComments,'countMembers'=>$countMembers);
      }
      return $all;
    }

    public static function groupDetails($userId,$groupId) {
      $groupMembers = Groupmembers::groupMembers($userId,$groupId);
      $groupDetails = Groups::select('groups.*','users.firstName','users.lastName')
      ->leftJoin('users','groups.adminId','=','users.id')->where('groups.id',$groupId)->first();
      $groupIcon = User::getMainURL()."photos/".$groupDetails->groupIcon;
      $adminName = $groupDetails->firstName." ".$groupDetails->lastName;
      $groupSavings = Savings::select('savings.*','users.firstName','users.lastName')
      ->leftJoin('users','savings.userId','=','users.id')->where('savings.groupId',$groupId)->orderBy('id','DESC')->limit(20)->get();
      return array('groupName'=>$groupDetails->groupName,'groupIcon'=>$groupIcon,'description'=>$groupDetails->description,'groupTypeId'=>$groupDetails->groupTypeId,'groupTypeName'=>$groupDetails->groupTypeName,'capacity'=>$groupDetails->capacity,'adminName'=>$adminName,'groupMembers'=>$groupMembers,'groupSavings'=>$groupSavings);
    }

}
