<?php

namespace App;
use App\Responseobject;
use Response;
use Illuminate\Database\Eloquent\Model;

class Loantypes extends Model
{

    protected $table = 'loantypes';

    public static function GetAll() {
      return Loantypes::where('isDeleted',0)->get();
    }

}
