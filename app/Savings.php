<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use Response;
use Log;

class Savings extends Model
{
    protected $table = 'savings';

    public static function mySavings($userId) {
      $data = Savings::select('savings.*','groups.groupName','groups.groupIcon')
      ->leftJoin('groups','savings.groupId','=','groups.id')
      ->where('savings.userId',$userId)
      ->where('savings.isDeleted',0)->get();
      $all = array();
      foreach ($data as $key) {
        $groupIcon = User::getMainURL()."photos/".$key->groupIcon;
        $date = self::convertDate($key->created_at);
        $all[] = array('id'=>$key->id,'amount'=>$key->amount,'date'=>$date,'groupName'=>$key->groupName,'groupIcon'=>$groupIcon);
      }
      return $all;
    }

    public static function convertDate($date) {
      return date("d-m-Y H:i A", strtotime($date));
    }

}
