<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('requestOtp', 'customer\AccountController@requestOtp');
Route::post('verifyOtp', 'customer\AccountController@verifyOtp');
Route::post('register', 'customer\AccountController@register');
Route::post('login', 'customer\AccountController@login');
Route::post('forgotPass', 'customer\AccountController@forgotPass');
Route::post('changePassword', 'customer\AccountController@changePassword');
Route::post('editProfile', 'customer\AccountController@editProfile');

Route::post('checkVersion', 'GeneralController@checkVersion');

Route::get('getNotifications/{userId}/{page}', 'NotificationsController@getNotifications');

Route::get('loadCounties', 'GeneralController@loadCounties');
Route::get('loadWards/{countyId}', 'GeneralController@loadWards');

Route::post('requestSTKpush', 'C2BController@requestSTKpush');

Route::post('uploadProfilePhoto', 'customer\DashboardController@uploadProfilePhoto');
Route::post('uploadPhoto', 'customer\DashboardController@uploadPhoto');
Route::post('removePhoto', 'customer\DashboardController@removePhoto');
Route::post('groupTypes', 'customer\DashboardController@groupTypes');
Route::post('joinGroup', 'customer\DashboardController@joinGroup');
Route::post('groupDetails', 'customer\DashboardController@groupDetails');
Route::post('exitGroup', 'customer\DashboardController@exitGroup');
Route::post('createGroup', 'customer\DashboardController@createGroup');
Route::post('editGroup', 'customer\DashboardController@editGroup');
//Route::post('uploadGroupPhoto', 'customer\DashboardController@uploadGroupPhoto');

Route::post('mySavings', 'customer\DashboardController@mySavings');
Route::post('myPhotos', 'customer\DashboardController@myPhotos');

Route::post('groups', 'customer\DashboardController@groups');
Route::post('myGroups', 'customer\DashboardController@myGroups');
Route::post('groupMembers', 'customer\DashboardController@groupMembers');
Route::post('myProfile', 'customer\DashboardController@myProfile');

Route::post('loanTypes', 'customer\DashboardController@loanTypes');
Route::post('applyLoan', 'customer\DashboardController@applyLoan');
Route::post('myLoans', 'customer\DashboardController@myLoans');
Route::post('groupChats', 'customer\DashboardController@groupChats');
Route::post('postChat', 'customer\DashboardController@postChat');

Route::post('sendInvite', 'customer\DashboardController@sendInvite');

Route::post('elect', 'customer\DashboardController@elect');
Route::post('election', 'customer\DashboardController@election');

Route::post('invite', 'customer\DashboardController@invite');
Route::post('myInvites', 'customer\DashboardController@myInvites');
Route::post('actOnInvite', 'customer\DashboardController@actOnInvite');

//Route::post('memberProfile', 'customer\DashboardController@memberProfile');
